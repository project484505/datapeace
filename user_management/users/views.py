from django.shortcuts import render

# Create your views here.
from rest_framework import generics, filters
from .models import User
from .serializers import UserSerializer
from django_filters.rest_framework import DjangoFilterBackend

class UserListCreateAPIView(generics.ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    filter_backends = [DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter]
    filterset_fields = ['age']
    search_fields = ['first_name', 'last_name']
    ordering_fields = ['age']

class UserRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

# User Management API

## Setup

1. Clone the repository:
   ```bash
   git clone <repository_url>
   cd user_management



2. Create and activate a virtual environment:

python -m venv venv
source venv/bin/activate  # On Windows use `venv\Scripts\activate`


Install dependencies:

pip install -r requirements.txt


3. Apply migrations:

python manage.py makemigrations
python manage.py migrate


4. Run the development server:

python manage.py runserver




5. API Endpoints

GET /api/users: List users with pagination, searching, and sorting.
POST /api/users: Create a new user.
GET /api/users/{id}: Get details of a specific user.
PUT /api/users/{id}: Update a user's details.
DELETE /api/users/{id}: Delete a user.
